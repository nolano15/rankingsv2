package rankings;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Handles standard mathematical computations
 * 
 * @author Nolan O'Shea
 */
public class Stats {
	double isoSD, havocSD, foSD;

    /**
     * Constructs Stats from a TeamStream
     * 
     * @param teams TeamStream to compute Stats over
     */
    public Stats(final Collection<Team> teams) {
    	// map each stat
    	this.isoSD = getStdDev(teams.parallelStream().map(Team::getIso));
    	this.havocSD = getStdDev(teams.parallelStream().map(Team::getHavoc));
    	this.foSD = getStdDev(teams.parallelStream().map(Team::getFo));
    }

    static double getMean(final Collection<Double> vals) {
    	return vals.parallelStream()
    			.mapToDouble(x -> x)
    			.average()
    			.getAsDouble(); // unwrap OptionalDouble
    }
    
    double getVariance(final Collection<Double> vals) {
        double mean = getMean(vals);
        return vals.parallelStream()
        		.mapToDouble(x -> (x - mean)*(x - mean))
                .sum()/vals.size();
    }

    double getStdDev(final Stream<Double> vals) {
        return Math.sqrt(getVariance(vals.collect(Collectors.toList())));
    }
}
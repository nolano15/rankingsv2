package rankings;

import java.util.List;

/**
 * Struct containing a team's name, stats, and score
 * 
 * @author Nolan O'Shea
 */
public class Team {
	String name;
	private double score, fo, iso, havoc;
	private Stats stats; // composition

	public Team(String name) {
		// initial value
		this.score = Double.NEGATIVE_INFINITY;
		
		// nicknames
		this.name = "Missouri".equals(name) ?
				"Mizzou" : "Washington State".equals(name) ?
						"Wazzu" : "Ohio State".equals(name) ?
								name.toLowerCase() : name;
	}
	
	/**
	 * Calculates and returns team ratings
	 * 
	 * @param s stats to calc with
	 * 
	 * @return team rating as std devs above or below avg
	 */
	double getScore() {
		// lazy + less nesting
		if(score != Double.NEGATIVE_INFINITY) {
		    return score;
		}
		
		final int SP_AVG = 100;
		final double HAVOC_AVG = 16.1; // havoc updates weekly
			
		double o, d;
		
		// explosiveness and havoc
		o = (iso - SP_AVG)/stats.isoSD;
		d = (havoc - HAVOC_AVG)/stats.havocSD;
		
		score = .6*o + .4*d;
		score = score*1/3 + (2.0/3)*fo/stats.foSD; // FO bias
		score *= stats.foSD; // scale to FO
		
		return score;
	}
	
	/**
     * Finds the nearest centroid to this Team
     * 
     * @param clusters clusters to search through
     * 
     * @return ndx of nearest centroid
     */
    int nearestCentroid(final List<TeamCluster> clusters) {
        int minNDX = -1;
        double dist, minDist = Double.POSITIVE_INFINITY;
        
        // find the closest centroid
        for(int i = 0; i < clusters.size(); i++) {
        	var cluster = clusters.get(i);
        	
        	// abs since no need to square
        	dist = Math.abs(getScore() - cluster.centroid);
        	
            if(dist < minDist) {
            	// update min vals
                minDist = dist;
                minNDX = i;
            }
        }    
        return minNDX;
    }
    
    @Override
	public String toString() {
		return String.format("%-17s %.2f\r\n", name, score);
	}

	public Team setStats(Stats stats) {
    	this.stats = stats;
    	return this; // mapping
    }

	public void setFo(double fo) {
		this.fo = fo;
	}

	public void setIso(double iso) {
		this.iso = iso;
	}

	public void setHavoc(double havoc) {
		this.havoc = havoc;
	}

    public double getIso() {
        return iso;
    }

    public double getHavoc() {
		return havoc;
	}

    public double getFo() {
        return fo;
    }
}
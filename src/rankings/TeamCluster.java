/**
 * @author Nolan O'Shea
 */

package rankings;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TeamCluster {
	private List<Team> teams;
	double centroid;
	
	public TeamCluster() {
        this.teams = new ArrayList<Team>();
    }
	
	/**
	 * Constructs TeamCluster and initializes centroid
	 * 
	 * @param teams Teams in this cluster
	 */
	public TeamCluster(final List<Team> teams) {
        this.teams = new ArrayList<Team>(teams);
        centroid();
    }
	
    /**
     * Computes the mean point of the Teams in this TeamCluster as a centroid
     */
    void centroid() {
    	// map to Team Score
    	centroid = Stats.getMean(teams.stream()
    			.map(Team::getScore)
    			.collect(Collectors.toList()));
    }

	public List<Team> getTeams() {
		// encapsulation
		return new ArrayList<Team>(teams);
	}
	
	public void add(Team t) {
		this.teams.add(t);
	}
}

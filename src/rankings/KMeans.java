package rankings;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * @author Nolan O'Shea
 */
public class KMeans {
	private static List<TeamCluster> clusters;
	
    /**
     * Clusters Teams by KMeans
     * 
     * @param teams Teams to be clustered
     * 
     * @return clusters
     */
    static List<TeamCluster> cluster(final List<Team> teams) {
        final int K = 5, // 5 clusters
        		  INIT_PART = 5; // initially 5 Teams per cluster
        
    	// initialize clusters data structure
        clusters = new ArrayList<TeamCluster>(K);
        
        // initialize k clusters, with initial partition of INIT_PART
        IntStream.range(0, K).forEach(i ->
        	clusters.add(new TeamCluster(teams.subList(i*INIT_PART, (i + 1)*INIT_PART))));
        
        boolean again;
        do {
            again = false;
            
            // Computes the mean point of the Teams in each cluster as centroid
            clusters.forEach(TeamCluster::centroid);
            
            // Generate k new clusters
            final var clustersNew = new ArrayList<TeamCluster>(K);    
            IntStream.range(0, K).forEach(i -> clustersNew.add(new TeamCluster()));
            
            // Assign each Team to the nearest centroid
            teams.stream().forEach(t ->
            	// adds team to index of nearest centroid
            	clustersNew.get(t.nearestCentroid(clusters)).add(t));
            
            // for all k clusters
            // breaks loop as soon as an update is detected
            for(int i = 0; i < K; i++) {
            	// Clusters being compared this iteration
                List<Team> clusterA = clustersNew.get(i).getTeams(),
                           clusterB = clusters.get(i).getTeams();
                
                // if old and new clusters of Teams are not equivalent, then break
                if(!(clusterA.containsAll(clusterB) && clusterB.containsAll(clusterA))) {
                    again = true;
                    break;
                }
            }
            
            // prep for next iteration
            if(again) {
                clusters = clustersNew;
            }
        } while(again); // While there is no change of the teams in each cluster       
        return clusters;
    }
}
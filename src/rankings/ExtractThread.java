package rankings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.function.ObjDoubleConsumer;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parallel Processing and Functional Interfaces
 * 
 * @author Nolan O'Shea
 */
public class ExtractThread implements Runnable {
	// Filters marked-up ampersands
	final UnaryOperator<String> ampFilter = s -> s.replace("&amp;", "&");
	
	private ObjDoubleConsumer<Team> strat; // contains unique line of code
	private BufferedReader in;
	private int skip; // lines to skip
	Matcher matcher;
	
	public ExtractThread(BufferedReader in, int skip, ObjDoubleConsumer<Team> strat) {
		this.in = in;
		this.skip = skip;
		this.strat = strat;
	}
	
	public ExtractThread(String url, int skip, ObjDoubleConsumer<Team> strat) throws IOException {
		// get URL from String
		this(getURL(url), skip, strat);
	}

	// Extracts team data from URL stream
	@Override
	public void run() {
		try {
			final int TEAM_COUNT = 130; // FBS
			
			for(int i = 0; i < TEAM_COUNT;) {
				if((matcher = Pattern.compile("<td>(.*)</td>").matcher(in.readLine())).find()) {
					
					String line = ampFilter.apply(matcher.group(1));
					
					// if team name set contains line, if this is a team's header
					if(Script.teamStats.keySet().contains(line)) {						
					    // skip custom number of lines
						for(int j = 0; j < skip; j++) {
						    in.readLine();
						}					
						// call custom update method
						strat.accept(Script.teamStats.get(line), findDouble(in));
						i++;
					}
				}
			}
		} catch(IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Finds the next double value from input stream using RegEx
	 * 
	 * @param in input stream
	 * 
	 * @return parsed double value
	 * 
	 * @throws IOException
	 *     if I/O problem
	 */
	double findDouble(BufferedReader in) throws IOException {
		while(!(matcher = Pattern.compile("(-?[0-9]+\\.[0-9]+)").matcher(in.readLine())).find());
		return Double.parseDouble(matcher.group(1));
	}
	
	/**
	 * Factory method
	 * 
	 * @param url String url addr
	 * 
	 * @return URL input stream
	 * 
	 * @throws IOException
	 * 		if no internet
	 */
	public static BufferedReader getURL(String url) throws IOException {
		return new BufferedReader(new InputStreamReader(new URL(url).openConnection().getInputStream()));
	}
}

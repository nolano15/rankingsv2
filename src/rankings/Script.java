package rankings;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Nolan O'Shea
 */
public class Script {
	// main data structure
	public static Map<String, Team> teamStats;
	
	public static void main(String args[]) throws IOException {
		final String URL = "https://www.footballoutsiders.com/stats/", // root
					 FO_IN = URL + "fplus",
					 ISO_IN = URL + "ncaaoff",
					 HAVOC_IN = URL + "ncaadef";
		
		// load will populate Team dictionary
		load();
		
		// MULTITHREADING
		System.out.println("Extracting F/+...");
		new Thread(new ExtractThread(FO_IN, 0, (t, d) -> t.setFo(d))).start();
		
		System.out.println("Extracting IsoPPP+...");
		new Thread(new ExtractThread(ISO_IN, 3, (t, d) -> t.setIso(d))).start();
		
		System.out.println("HAVOC!!!...");
		var in = ExtractThread.getURL(HAVOC_IN);
		
		// first call has empty update to skip lines
		new Thread(new ExtractThread(in, 5, (t, d) -> {})).start();
		
		// block join
		// must skip lines before spawning second havoc thread
		while(Thread.activeCount() > 1);
		
		new Thread(new ExtractThread(in, 5, (t, d) -> t.setHavoc(d))).start();
		
		// block join
		while(Thread.activeCount() > 1);
		
		save();
	}
	
	/**
	 * Loads a KeySet from database
	 * 
	 * @throws IOException
	 * 		if file not found
	 */
	private static void load() throws IOException {
		final String TEAM_DIC = "teamDictionary.txt"; // team names database
		
		teamStats = new HashMap<String, Team>();
		
		// read from database
		try(var in = new BufferedReader(new FileReader(TEAM_DIC))) {
			String name;
			
			// populate dictionary
			System.out.println("Loading...");
			while((name = in.readLine()) != null) {
			    teamStats.put(name, new Team(name));
			}
		}
	}
	
	/**
	 * Computes final results and saves to FILE
	 * 
	 * @throws FileNotFoundException
	 */
	private static void save() throws FileNotFoundException {
		System.out.println("Calculating...");
		
		// compute Stats
		var stats = new Stats(teamStats.values());
		
		final String FILE_OUT = "rankings.txt";
		final int TOP = 25; // top 25
		
		// LAMBDA
		try(var out = new PrintWriter(new BufferedWriter(new PrintWriter(FILE_OUT)))) {		
			// apply clustering algorithm
			var clusters = KMeans.cluster(teamStats.values().parallelStream()
				.map(t -> t.setStats(stats)) // apply Stats to each Team
				.sorted((t1,  t2) -> Double.compare(t2.getScore(), t1.getScore())) // sort by Score
				.limit(TOP)
				.collect(Collectors.toList()));

			int i = 0;
			// for each cluster
			for(TeamCluster cluster : clusters) {
				// for each team
				for(Team t : cluster.getTeams()) {
					out.printf("%2d) " + t, ++i);
				}
				out.println(); // separate clusters
			}
		    System.out.println("Done!");
		}
	}
}

